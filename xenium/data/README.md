# Description of data

```
data
├── sc_xe_lognorm_meta.rds                     ## An R list with normalized (log) single cell and Xenium data and the corresponding metadata
├── spf1_cell_boundaries.csv.gz                ## Cell boundary for Xenium roll of SPF mouse
├── xenium_cell_euclidean30NN_assign.all.rds   ## Cell type assignment to Xenium cells based 30-nearest neighbors in single cell data
└── xenium_unroll_coord.csv.gz                 ## Unrolled coordniates of Xenium rolls
```