# Analysis scripts, tables and figures used for the mouse gut molecular cartography project

## Description

This repository hosts the scripts used to analyze the spatial and single cell transcriptomic datasets generated from mouse intestine. 

## Data & Analysis

Processed data and scripts can be found in the corresponding folders ([visium](./visium/data/README.md), [xenium](./xenium/data/README.md), [singlecell](./singlecell/data/README.md), flow).

### Raw Sequencing data (GEO: GSE245316):
 - [Visium raw/GSE245274](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE245274)
 - [Single cell raw/GSE245315](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE245315)

### Raw Xenium data (zenodo):
 - [SPF and GF colon](https://doi.org/10.5281/zenodo.14226737)
 - [ILC2 WT and KO colon](https://doi.org/10.5281/zenodo.14226406)

### Processed data:
 - Visium
   - [DSS experiment](./visium/data/paper_dss_rolls.rds)
   - [Microbiota experiment](./visium/data/paper_spf_gf_fmt_rolls.rds)
 - Xenium
   - [Transformed counts with spatial coordinates](./xenium/data/sc_xe_lognorm_meta.rds)
   - [Cell type annotation](./xenium/data/xenium_cell_euclidean30NN_assign.all.rds)
 - Single cell
   - [Integrated single cell data](./singlecell/data/20240219_unified_annotated.diet.rds)
   - [Integrated velocyto data](./singlecell/data/velocyto_data.rds)

> Data processed with `Seurat V4.3`, `SeuratObject V4.1.3`


### Single cell portal:
 - [Visium - microbiota experiemnts](https://singlecell.broadinstitute.org/single_cell/study/SCP2771/molecular-cartography-reveals-robust-adaptable-and-resilient-intestinal-networks-visium-data-dss-experiment)
 - [Visium - DSS experiments](https://singlecell.broadinstitute.org/single_cell/study/SCP2762/molecular-cartography-reveals-robust-adaptable-and-resilient-intestinal-networks-visium-data)
 - [Single cell - integrated](https://singlecell.broadinstitute.org/single_cell/study/SCP2760/molecular-cartography-reveals-robust-adaptable-and-resilient-intestinal-networks-single-cell-data)
 
### Scripts:

Main figures:

 - Fig. 1: [spf_expression_overview.Rmd](./visium/script/spf_expression_overview.Rmd), [spf_genes_shared_across_si_colon.Rmd](./visium/script/spf_genes_shared_across_si_colon.Rmd), [spatial_biology_SLC.Rmd](./visium/script/spatial_biology_SLC.Rmd)
 - Fig. 2: [expression_deg_driven_by_microbiome.Rmd](./visium/script/expression_deg_driven_by_microbiome.Rmd)
 - Fig. 3: [expression_recovery_after_dss.Rmd](./visium/script/expression_recovery_after_dss.Rmd), [spearman_correlation_DSS.Rmd](./visium/script/spearman_correlation_DSS.Rmd)
 - Fig. 4: [singlecell_composition_and_marker.Rmd](./singlecell/script/singlecell_composition_and_marker.Rmd), [xenium_colon_cropped.Rmd](./xenium/script/xenium_colon_cropped.Rmd), [xenium_colon.Rmd](./xenium/script/xenium_colon.Rmd)
 - Fig. 5: [singlecell_spf_vs_gf_celltype_segment.Rmd](./singlecell/script/singlecell_spf_vs_gf_celltype_segment.Rmd), [xenium_colon.Rmd](./xenium/script/xenium_colon.Rmd)
 
Extended figures:

 - ED. 1: [spf_expression_overview.Rmd](./visium/script/spf_expression_overview.Rmd)
 - ED. 2: [enrichment_analysis.Rmd](./visium/script/enrichment_analysis.Rmd), [spatial_biology_gpcr.Rmd](./visium/script/spatial_biology_gpcr.Rmd), [spatial_biology_SLC.Rmd](./visium/script/spatial_biology_SLC.Rmd), [spatial_biology_ibd.Rmd](./visium/script/spatial_biology_ibd.Rmd)
 - ED. 3: [spatial_biology_ibd.Rmd](./visium/script/spatial_biology_ibd.Rmd), [spatial_biology_TFs.Rmd](./visium/script/spatial_biology_TFs.Rmd)
 - ED. 4: [expression_deg_driven_by_microbiome.Rmd](./visium/script/expression_deg_driven_by_microbiome.Rmd), [expression_polarity_robust_to_microbiome.Rmd](./visium/script/expression_polarity_robust_to_microbiome.Rmd), [expression_polarity_robust_to_circadian.Rmd](./visium/script/expression_polarity_robust_to_circadian.Rmd)
 - ED. 5: [enrichment_analysis.Rmd](./visium/script/enrichment_analysis.Rmd)
 - ED. 6: [enrichment_analysis.Rmd](./visium/script/enrichment_analysis.Rmd), [expression_recovery_after_dss.Rmd](./visium/script/expression_recovery_after_dss.Rmd)
 - ED. 7: [singlecell_composition_and_marker.Rmd](./singlecell/script/singlecell_composition_and_marker.Rmd)
 - ED. 8: [xenium_colon.Rmd](./xenium/script/xenium_colon.Rmd), [singlecell_velocyto.Rmd](./singlecell/script/singlecell_velocyto.Rmd)
 - ED. 9: [singlecell_spf_vs_gf_celltype_segment.Rmd](./singlecell/script/singlecell_spf_vs_gf_celltype_segment.Rmd),
 - ED. 10: [xenium_colon.Rmd](./xenium/script/xenium_colon.Rmd)
 

## Citation

Toufic Mayassi, Chenhao Li, Åsa Segerstolpe, Eric Brown, Rebecca Weisberg, Toru Nakata, Hiroshi Yano, Paula Herbst, David Artis, Daniel Graham, Ramnik Xavier. Spatially restricted immune and microbiota-driven adaptation of the gut. [*Nature*](https://doi.org/10.1038/s41586-024-08216-z). (2024). 
