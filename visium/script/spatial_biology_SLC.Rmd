---
title: "Gene visualization on unroll - SLC genes"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


> Memory recommended for this script: 32Gb

Load libraries
```{r message=FALSE, warning=FALSE}
library(tidyverse)
library(Seurat)
library(ggstar)
library(reshape2)
library(ComplexHeatmap)
library(circlize)
library(igraph)
library(ggraph)
library(foreach)
library(ggridges)
```

Load data
```{r message=FALSE, warning=FALSE}
## Unroll
aligned <- read_csv("../data/unroll_aligned_w_seg.csv.gz")

## Marker genes
deg.si.markers <- read_csv("../data/marker_genes_tissue_si_deg.csv.gz") %>% 
  filter(str_detect(gene, "^Rpl|^Rps|^mt-", negate = T))
deg.colon.markers <- read_csv("../data/marker_genes_tissue_subseg_deg.csv.gz") %>% 
  filter(str_detect(gene, "^Rpl|^Rps|^mt-", negate = T)) %>% 
  filter(str_detect(cluster, "Colon"))

## Seurat object
spf <- readRDS("../data/paper_spf_gf_fmt_rolls.rds") %>% subset(microbes=="SPF") %>% 
  NormalizeData() %>% ScaleData() 

meta2add <- 
  mutate(aligned, tissue.seg=ifelse(tissue!="Colon", tissue, interval)) %>% 
  mutate(tissue.seg=str_replace_all(tissue.seg, c("Colon_1"="Colon proximal", "Colon_2"="Colon medial", "Colon_3"="Colon distal"))) %>% 
  select(id, tissue.seg) %>% 
  data.frame(row.names = "id")

spf <- AddMetaData(spf, meta2add)

## Reduced size gene list with high average expression
filtered.genes <- which(rowSums(AverageExpression(spf,assays = "Spatial", 
                                                            group.by = "tissue")[[1]] > 0.5)>=1) %>% names

gc()
```

Constants

```{r}
colors <- c("#E64B35FF","#4DBBD5FF","#00A087FF","#3C5488FF","#F39B7FFF","#8491B4FF","#91D1C2FF","#B09C85FF","#FAFD7CFF","#82491EFF","#B7E4F9FF","#FB6467FF","#526E2DFF","#E762D7FF","#FAE48BFF","#A6EEE6FF","#95CC5EFF")
heat.color <- circlize::colorRamp2(c(-2, 0, 2), c("#1C4573", "yellow", "#ED3E30"))
tissue.order <- c("Duodenum", "Jejunum1", "Jejunum2", "Ileum", "Colon proximal", "Colon medial", "Colon distal")
```

Functions
```{r}
z.na <- function(x) {
  x[x==0] <- NA
  (x-mean(x, na.rm=T))/sd(x, na.rm = T)
}
```

### Small SLC set

```{r}
section.genes <- read_csv("../data/segment_shared_genes.csv.gz") ## exported from script `spf_genes_shared_across_si_colon.Rmd`
slcs <- filter(section.genes, str_detect(gene, "^Slc"))

receptors <- 
  filter(slcs, str_detect(group, ",")) %>% 
  filter(str_detect(group, "C") & str_detect(group, "[DJI]"))

receptors$group <- factor(receptors$group, levels=c("D,C1", "D,C2", "D,C3" , "J1,J2,C3", "J1,C3", "J2,C2", "J2,C3", "I,C1", "I,C3"), ordered = T)
gene.list <- arrange(receptors, group)
```

```{r}
plot.dat.raw <- FetchData(spf, vars=c("tissue.seg", gene.list$gene), slot = "data") %>%
  merge(aligned, by.x=0, by.y=1) 

plot.dat <- plot.dat.raw %>% 
  select(-Row.names, -sample_short_name, -tissue, -unroll_y, -mouse, -microbes, -y_layer, -interval) %>% 
  select(tissue=tissue.seg, everything()) %>% 
  group_by(unroll_x_align, tissue) %>% 
  summarise_all(mean)

heat.dat <- arrange(plot.dat, unroll_x_align) %>% 
  filter(!is.na(unroll_x_align))
mat <- apply(heat.dat[,-c(1,2)], 2, function(x) z.na(x)) %>% t()
mat[mat>2] <- 2
mat[mat< -2] <- -2
```


```{r fig.height=3, fig.width=10}
colAnn <- HeatmapAnnotation(Tissue = heat.dat$tissue,
                            col = list(Tissue = c("Duodenum" = colors[1], 
                                                  "Jejunum1" = colors[2], 
                                                  "Jejunum2" = colors[3], 
                                                  "Ileum" = colors[4],
                                                  "Colon proximal" = colors[6],
                                                  "Colon medial" = colors[7],
                                                  "Colon distal" = colors[8])
                                       ),
                            annotation_legend_param = list(Tissue = list(title_position = "lefttop-rot", at=c("Duodenum", "Jejunum1", "Jejunum2", "Ileum", "Colon proximal", "Colon medial", "Colon distal")))
                            )

h <- Heatmap(mat, use_raster=F, cluster_columns = F, 
        col= heat.color,
        show_row_names = T,
        heatmap_legend_param = list(legend_height = unit(4, "cm"),
                                    title_position = "lefttop-rot",
                                    title="Expression"
                                    ),
        top_annotation = colAnn,
        split = gene.list$group,
        row_title_rot = 0,
        cluster_rows = F,
        )

draw(h, heatmap_legend_side = "left", annotation_legend_side="left")
```




### Cord diagram of SLC sharing

```{r}
## criteria following `spf_genes_shared_across_si_colon.Rmd`
gene.sharing <- merge(
  filter(deg.si.markers,  avg_log2FC>0.25, pct.1>0.3) %>% select(cluster, gene),
  filter(deg.colon.markers, avg_log2FC>0.25, pct.1>0.3) %>% 
    mutate(cluster=str_replace_all(cluster, c("Colon_1"="Colon proximal", "Colon_2"="Colon medial", "Colon_3"="Colon distal"))) %>% 
    select(cluster, gene),
  by="gene"
) 

# Transform input data in a adjacency matrix
adjacencyData <- 
  mutate(gene.sharing, cluster.x=factor(cluster.x, levels=(tissue.order)),
       cluster.y=factor(cluster.y, levels=rev(tissue.order))
       ) %>% 
  with( table(cluster.y, cluster.x))
```


```{r fig.height=5.5, fig.width=5.5}
grid.col = c("Duodenum" = colors[1], 
           "Jejunum1" = colors[2], 
           "Jejunum2" = colors[3], 
           "Ileum" = colors[4],
           "Colon proximal" = colors[6],
           "Colon medial" = colors[7],
           "Colon distal" = colors[8])

circos.clear()
# Make the circular plot
chordDiagram(adjacencyData, transparency = 0.5, #group=group,
                 annotationTrack = c("grid"),
             annotationTrackHeight = 0.1,
             grid.col = grid.col,
    preAllocateTracks = list(
        track.height = mm_h(4),
        track.margin = c(mm_h(4), 0)
  )
  )


circos.track(track.index = 2, panel.fun = function(x, y) {
    sector.index = get.cell.meta.data("sector.index")
    xlim = get.cell.meta.data("xlim")
    ylim = get.cell.meta.data("ylim")
    circos.text(mean(xlim), mean(ylim), sector.index, col="white", 
                cex = 1, niceFacing = TRUE, facing = "bending")
}, bg.border = NA)
```

### SLC-Ligand link


```{r}
slc.genes <- union(filter(deg.colon.markers, avg_log2FC>0.5)$gene, 
                   filter(deg.si.markers, avg_log2FC>0.5)$gene) %>% str_subset("^Slc") 

slc.genes <- c(slc.genes, "Slc39a8")

classes <- read_csv("../data/iMusta4SLC.csv.gz") %>% 
  mutate(`SLC name`=str_to_sentence(`SLC name`)) %>% 
  filter(`SLC name` %in% slc.genes)

slc.genes <- 
  intersect(slc.genes, unique(classes$`SLC name`))

## manually annotated SLC ligand type
anno <- read_csv("../data/slc_selected_anno.csv.gz")
```

```{r}
plot.dat.slc.genes <- 
  FetchData(spf, vars=slc.genes, slot = "data") %>%
  merge(aligned, by.x=0, by.y=1) %>% 
  select(-Row.names, -sample_short_name, -unroll_y, -mouse, -microbes, -y_layer, -interval) %>% 
  group_by(unroll_x_align, tissue) %>% 
  summarise_all(mean)
heat.dat.slc.genes <- arrange(plot.dat.slc.genes, unroll_x_align) %>% 
  filter(!is.na(unroll_x_align))
mat <- apply(heat.dat.slc.genes[,-c(1,2)], 2, function(x) z.na(x)) %>% t()
mat[mat>2] <- 2
mat[mat< -2] <- -2
```


```{r fig.height=6, fig.width=7}
colAnn <- HeatmapAnnotation(Tissue = heat.dat$tissue,
                            col = list(Tissue = c("Duodenum" = colors[1], 
                                                  "Jejunum1" = colors[2], 
                                                  "Jejunum2" = colors[3], 
                                                  "Ileum" = colors[4],
                                                  "Colon proximal" = colors[6],
                                                  "Colon medial" = colors[7],
                                                  "Colon distal" = colors[8])
                                       ),
                            annotation_legend_param = list(Tissue = list(title_position = "lefttop-rot", at=c("Duodenum", "Jejunum1", "Jejunum2", "Ileum", "Colon proximal", "Colon medial", "Colon distal")))
                            )

h <- Heatmap(mat, use_raster=T, raster_quality = 5,  cluster_columns = F, 
        col= heat.color,
        show_row_names = T,
        heatmap_legend_param = list(legend_height = unit(4, "cm"),
                                    title_position = "lefttop-rot",
                                    title="Expression"
                                    ),
        top_annotation = colAnn
        )
## Tag:ED2b
draw(h, heatmap_legend_side = "left", annotation_legend_side="left")
```


```{r fig.height=8, fig.width=5}
order <- data.frame(slc=rownames(mat)[row_order(h)]) %>% 
  mutate(order=1:n())

g <- 
  distinct(classes, slc=`SLC name`, ligand=`Ligand name`) %>% 
  filter(slc %in% rownames(mat)) %>% 
  mutate(ligand = ifelse(is.na(ligand), "unknown", ligand)) %>%
  merge(order, by=1) %>% arrange(desc(order)) %>% 
  graph.data.frame()  


V(g)$type[V(g)$name %in% classes$`SLC name`] <- "Slc"
V(g)$type[!V(g)$name %in% classes$`SLC name`] <- "ligand"
V(g)$anno <- "gene"
V(g)[V(g)$type=="ligand"]$anno <- data.frame(anno, row.names = 1)[V(g)[V(g)$type=="ligand"]$name ,]

g <- delete_edges(g, which(as_edgelist(g)[,2]=="unknown"))
g <- delete_vertices(g, "unknown")

layout <- create_layout(g, layout = 'linear') 

layout$x <- group_by(layout, type) %>% 
  mutate(x=1:n()) %>% pull(x)

layout$y <- ifelse(layout$type=="ligand", 2, 0)
layout$x <- ifelse(layout$type=="ligand", layout$x, layout$x*1.7)

## Tag:ED2b
ggraph(g, layout=layout) +
  geom_edge_diagonal(alpha=0.2) +
  geom_node_point(aes(color=anno), size=4) +
  geom_node_text(aes(filter=type =="ligand", label=name,y = y+0.2),  hjust=0, size=3) +
  coord_flip() +
  scale_color_manual(values = colors[-1]) + 
  ylim(-1,6) + 
  theme_void()
```


### Smaller set of SLCs

```{r fig.height=6, fig.width=6}
plot.dat <- select(plot.dat.raw, -Row.names, -sample_short_name, -tissue, -microbes, -y_layer, -interval) %>% 
  melt(id.vars=c("tissue.seg", "mouse", "unroll_x_align", "unroll_y")) %>% 
  merge(gene.list, by.x="variable", by.y="gene") 

aux <- function(x) {
  mod <- mgcv::gam(scale(value)~s(unroll_x_align, bs="cs"), data=x)
  idx <- sort(unique(x$unroll_x_align))
  data.frame(unroll_x_align=idx, value=predict(mod, data.frame(unroll_x_align=idx)))
}

smoothed <- foreach(var=unique(plot.dat$variable), .combine = rbind) %do% {
  tmp <- filter(plot.dat, variable==var) %>% 
    select(value, unroll_x_align) %>% aux
  mutate(tmp, variable=var)
}

smoothed <- merge(smoothed, gene.list, by.x="variable", by.y="gene") %>% 
  mutate(group=fct_rev(group)) 

anno.text <- distinct(smoothed, group, variable) %>% 
  group_by(group) %>% 
  summarise(genes=paste0(variable, collapse = "\n")) 

anno.boundary <- group_by(plot.dat, tissue.seg) %>% 
  filter(mouse=="SPF1") %>% 
  slice_max(unroll_x_align) %>% 
  distinct(tissue.seg, unroll_x_align) %>% 
  filter(!is.na(tissue.seg))

## Tag:Fig1c
ggplot(smoothed, aes(x=unroll_x_align, y=reorder(variable, as.numeric(group)), height=value, fill=group)) +
  geom_vline(xintercept = anno.boundary$unroll_x_align, color="grey", lty=2) +
  geom_ridgeline(min_height = -5, alpha=0.5, scale = 0.8) +
  labs(x=NULL, y=NULL) +
  theme_minimal()  +
  xlim(-1000, NA) +
  scale_y_discrete(position = "right") +
  scale_fill_manual(values=c("#E6C88C",  "#FF9999","#FFFF99", "#FF9AFE" ,"cyan", "#CCA6F3", "#98FF99", "#9998FF")) +
  theme(legend.position = "none", 
        panel.grid.minor = element_blank(), 
        panel.grid.major.x =element_blank(),
        axis.text.x = element_blank(),
        axis.text.y = element_text(size=20)
        ) 
  
```


```{r fig.height=8, fig.width=6}
classes <- 
  read_csv("../data/iMusta4SLC.csv.gz") %>% 
  mutate(`SLC name`=str_to_sentence(`SLC name`)) 

g.sub <- 
  distinct(classes, slc=`SLC name`, ligand=`Ligand name`) %>% 
  merge(distinct(smoothed, variable, group), by=1, all.y=T) %>% 
  mutate(ligand = ifelse(slc %in% c("Slc51a", "Slc51b"), "bile acids", ligand)) %>% 
  mutate(ligand = ifelse(is.na(ligand), "unknown", ligand)) %>% 
  arrange((group)) %>% 
  graph.data.frame()  


V(g.sub)$type[str_detect(V(g.sub)$name, "Slc")] <- "Slc"
V(g.sub)$type[!str_detect(V(g.sub)$name, "Slc")] <- "ligand"
V(g.sub)$anno <- "gene"
V(g.sub)[V(g.sub)$type=="ligand"]$anno <- data.frame(anno, row.names = 1)[V(g.sub)[V(g.sub)$type=="ligand"]$name ,]


g.sub <- delete_edges(g.sub, which(as_edgelist(g.sub)[,2]=="unknown"))
g.sub <- delete_vertices(g.sub, "unknown")

layout <- create_layout(g.sub, layout = 'linear') 

layout$x <- group_by(layout, type) %>% 
  mutate(x=1:n()) %>% pull(x)

layout$y <- ifelse(layout$type=="ligand", 0.5, 0)
layout$x <- ifelse(layout$type=="ligand", layout$x, layout$x*1.8)

## Tag:Fig1c
ggraph(g.sub, layout=layout) +
  geom_edge_diagonal(alpha=0.2) +
  geom_node_point(size=6, color="orange") +
  geom_node_text(aes(filter=type =="ligand", label=name,y = y+0.1),  hjust=0, size=5) +
  #geom_node_text(aes(filter=type =="Slc", label=name,y = y+0.1),  hjust=0, size=3) +
  coord_flip() +
  ylim(0,2) + 
  theme_void()
```
```{r}
sessionInfo()
```

