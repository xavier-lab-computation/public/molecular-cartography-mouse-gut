---
title: "Spatial recovery from DSS treatment"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

> Memory recommended for this script: 16Gb

## Prepare libraries and datasets

Load libraries

```{r  message=FALSE, warning=FALSE}
library(tidyverse)
library(Seurat)
library(patchwork)
library(ggrepel)
```

Load data

```{r  message=FALSE, warning=FALSE}
align.dss <- read_csv("../data/unroll_aligned_w_seg_dss.csv.gz")

full <- readRDS("../data/paper_dss_rolls.rds") 
DefaultAssay(full) <- "Spatial"
full <- NormalizeData(full) %>% 
  ScaleData()

full <- AddMetaData(full, 
                    select(align.dss, id, unroll_x_align, colon_segment=interval) %>%
                      data.frame(row.names=1)) 
gc()
```


## Recovery of section identity - module score of proximal (C1), middle (C2) and distal (C3) colon

On Swiss rolls

```{r fig.height=12, fig.width=12,  message=FALSE, warning=FALSE}
deg.colon.markers <- read_csv("../data/marker_genes_tissue_subseg_deg.csv.gz") %>% 
  filter(str_detect(gene, "^Rpl|^Rps|^mt-", negate = T)) %>% 
  filter(str_detect(cluster, "Colon")) %>% 
  filter(pct.1>0.5, avg_log2FC>0) %>% #pull(cluster)
  group_by(cluster) %>% 
  mutate(rank=1:n()) %>% 
  filter(rank<=50)  %>% 
  arrange(pct.2, desc(avg_log2FC))

markerset <- split(deg.colon.markers$gene, f=deg.colon.markers$cluster)

tmp <- AddModuleScore(full, features = markerset)

colnames(tmp@meta.data) <- str_replace_all(colnames(tmp@meta.data), "Cluster", "C")

## Tag:ED6h
SpatialFeaturePlot(tmp, features = c("C1", "C2", "C3")) & 
    scale_fill_gradient2(low="#4d3f72", high="gold", mid="white", midpoint = 0.6, limits=c(0,1.2), oob=scales::squish )
```

Recovery of mid colon

```{r fig.height=5, fig.width=16, message=FALSE, warning=FALSE}
## Tag:Fig.3c
x <- SpatialFeaturePlot(full, features = "Ang4", pt.size.factor = 2, combine = F, alpha = 1, slot = "data")

wrap_plots(x, ncol=4) & 
  scale_fill_gradient2(midpoint = 1.5, limits=c(0,3), low="#4d3f72", mid="white", high="gold", oob = scales::squish) 
```

On unrolled X axis

```{r fig.height=6, fig.width=3,  message=FALSE, warning=FALSE}
## Tag:Fig4d
select(tmp@meta.data, unroll_x_align, C1, C2, C3, time_point) |> 
  reshape2::melt(id.vars=c("unroll_x_align", "time_point")) |> 
  ggplot(aes(x=unroll_x_align, y=value, color=variable)) +
  ggrastr::geom_point_rast(alpha=0.05,size=0.1) + 
  geom_smooth(method="loess") + 
  labs(x="Unrolled X axis", y="Module score") + 
  facet_wrap(~time_point, ncol=1, strip.position = "right") + 
  scale_color_manual(values=c("orange", "#3C5488FF", "#4DBBD5FF"), name="Region") +
  theme_bw() +
  theme(legend.position="top")
```

## Recovery of expression profile from DSS treatment

Umap

```{r fig.height=15, fig.width=4}
## UMAP
cols <- c("#4DBBD5FF", "orange", "#3C5488FF", "#8491B4FF", "#91D1C2FF", "blue", "black", "#82491EFF", "#FB6467FF", "#526E2DFF", "#FAFD7CFF", "pink")

relevel <- c("Sham"="SPF Colon 5", "D12"="SPF Colon DSS D12 1", "D30"="SPF Colon DSS D30 1", "D73"="SPF Colon DSS D73 1")
full@meta.data$date <- fct_recode(fct_relevel(factor(full@meta.data$sample_short_name), relevel), !!!relevel)

p1 <- DimPlot(full, group.by = "SCT_snn_res.0.3", split.by = "date", label = F, repel = T, ncol=1, pt.size=3, cols = alpha(cols, 0.3)) +
  theme_void() + 
  theme(legend.position = "none", title = element_blank(), 
        strip.text = element_blank())

## Legend
p2 <- select(full@meta.data, date, SCT_snn_res.0.3) %>% 
  count(date, cluster= SCT_snn_res.0.3) %>% 
  group_by(date) %>% 
  mutate(fraction=n/sum(n)) %>% 
  ggplot(aes(x=0,y=fraction, fill=cluster)) + 
  geom_bar(stat="identity") +
  facet_wrap(~date, ncol=1) +
  scale_fill_manual(values=cols) +
  theme_void() +
  guides(fill = guide_legend(ncol = 1, override.aes = list(alpha = 1))) + 
  theme(strip.text = element_blank(), legend.position = "left")

## Tag:Fig4b
p2 + plot_spacer() + p1 + plot_layout(widths =c(1, 1, 10))
```

Spatial

```{r fig.height=40, fig.width=10}
## Tag:Fig4b
Idents(full) <- full$SCT_snn_res.0.3
hili <- lapply(0:11, function(x) WhichCells(full, idents=x))
names(hili) <- as.character(0:11)
tmp <- c(cols[1:12], "grey")
names(tmp) <- c(as.character(0:11), "no")
SpatialDimPlot(full, cells.highlight = hili, cols.highlight=tmp, pt.size.factor = 2) + 
  plot_layout( ncol=1) &
  ggtitle(NULL)  & theme(legend.position = "none")
```



## Recovery of DEGs

```{r message=FALSE, warning=FALSE}
deg.D12 <- read_csv("../data/dss_deg.d12_vs_d0.csv.gz")
deg.D30 <- read_csv("../data/dss_deg.d30_vs_d0.csv.gz")
deg.D73 <- read_csv("../data/dss_deg.d73_vs_d0.csv.gz")

fc.D12 <- read_csv("../data/dss_fc.d12_vs_d0.csv.gz")
fc.D30 <- read_csv("../data/dss_fc.d30_vs_d0.csv.gz")
fc.D73 <- read_csv("../data/dss_fc.d73_vs_d0.csv.gz")
```


```{r fig.height=5, fig.width=15}
plot.dat <- 
  filter(fc.D12, abs(avg_log2FC)>1) %>% 
  select(gene, colon.cluster, avg_log2FC) %>% 
  merge(select(fc.D30, gene, colon.cluster, avg_log2FC), by=c(1,2)) %>% 
  rename(avg_log2FC.D12=avg_log2FC.x, avg_log2FC.D30=avg_log2FC.y) %>% 
  merge(select(fc.D73, gene, colon.cluster, avg_log2FC), by=c(1,2)) %>% 
  rename(avg_log2FC.D73=avg_log2FC) %>% 
  mutate(group=ifelse(avg_log2FC.D12>0, "Up", "Down")) %>% 
  reshape2::melt(id.vars=c("gene", "colon.cluster", "group")) %>% 
  mutate(variable=str_remove(variable, "avg_log2FC.")) %>% 
  mutate(colon.cluster=str_remove(colon.cluster, "olon_")) %>% 
  mutate(value=ifelse(value< -3, -3, value))

## Tag:ED6c
ggplot(plot.dat, aes(x=variable, y=value, group=gene, color=group)) +
  geom_line(alpha=0.3,linewidth=1) +
  geom_point(data=filter(plot.dat, abs(value)>2, variable=="D12")) + 
  geom_text_repel(data=filter(plot.dat, abs(value)>2, variable=="D12"), aes(label=gene), max.overlaps = 100, segment.colour="grey", bg.color="white", size=6) +
  ## D30 or D72
  geom_point(data=filter(plot.dat, abs(value)>1.5, variable!="D12"), color="black") + 
  geom_text_repel(data=filter(plot.dat, abs(value)>1.5, variable!="D12"), color="black", aes(label=gene), max.overlaps = 100, size=6) +
  labs(x=NULL, y="log2 fold change compared to sham") + 
  facet_wrap(~colon.cluster) +
  scale_color_manual(values=c("blue", "red"), guide=F) +
  theme_classic() +
  theme(strip.background = element_blank())
```


Ido1 cluster vs non-Ido1 cluster & wound

```{r fig.height=6, fig.width=9, message=FALSE, warning=FALSE}
wae.top50 <- 
  read_tsv("../data/nakata_et_al_epi_markers.tsv.gz") %>% 
  filter(cluster=="Wound associated") %>% 
  slice_max(order_by=avg_log2FC, n = 50) %>% pull(gene)

ido1 <- read_csv("../data/dss_deg.ido1_pos_vs_neg_logfc0.csv.gz")
ido1[1,2] <- 1e-40

## Tag:ED6g
ggplot(ido1, aes(x=avg_log2FC, y=-log10(p_val), label=X1)) + 
  ggrastr::geom_point_rast(color="grey", alpha=0.3)  + 
  ggrastr::geom_point_rast(data=filter(ido1, avg_log2FC > 0 & p_val_adj < 0.05), color="red" ,size=2, alpha=0.3)  +
  ggrastr::geom_point_rast(data=filter(ido1, avg_log2FC < 0 & p_val_adj < 0.05), color="blue" ,size=2, alpha=0.3)  + 
  geom_point(data=filter(ido1, X1 %in% wae.top50), color="yellow")  + 
  labs(x="Average log2 fold change",y="-log10 P-value") + 
  geom_text_repel(data=filter(ido1, X1 %in% wae.top50 | (avg_log2FC > 1 & p_val_adj < 0.01)), max.overlaps = 100, bg.color="white") + 
  theme_bw()
```


```{r}
sessionInfo()
```


