---
title: "Visualization of shared genes"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

> Memory recommended for this script: 32Gb

## Prepare libraries and datasets

Load libraries

```{r message=FALSE, warning=FALSE}
library(tidyverse)
library(Seurat)
library(reshape2)
library(ComplexHeatmap)
```

Load data

```{r message=FALSE, warning=FALSE}
colors <- c("#E64B35FF","#4DBBD5FF","#00A087FF","#3C5488FF","#8491B4FF","#91D1C2FF","#B09C85FF")
deg.si.markers <- read_csv("../data/marker_genes_tissue_si_deg.csv.gz") %>% 
  filter(str_detect(gene, "^Rpl|^Rps|^mt-|^Cfd$", negate = T))
deg.colon.markers <- read_csv("../data/marker_genes_tissue_subseg_deg.csv.gz") %>% 
  filter(str_detect(gene, "^Rpl|^Rps|^mt-|^Cfd$", negate = T)) %>% 
  filter(str_detect(cluster, "Colon"))
aligned <- read_csv("../data/unroll_aligned_w_seg.csv.gz")

spf <- readRDS("../data/paper_spf_gf_fmt_rolls.rds") %>% NormalizeData() %>% 
  subset(microbes=="SPF") %>% ScaleData()
tissue.order <- c("Duodenum", "Jejunum1", "Jejunum2", "Ileum", "Colon proximal", "Colon medial", "Colon distal")

meta2add <- 
  mutate(aligned, tissue.seg=ifelse(tissue!="Colon", tissue, interval)) %>% 
  mutate(tissue.seg=str_replace_all(tissue.seg, c("Colon_1"="Colon proximal", "Colon_2"="Colon medial", "Colon_3"="Colon distal"))) %>% 
  select(id, tissue.seg) %>% 
  data.frame(row.names = "id")

spf <- AddMetaData(spf, meta2add)
gc()
```

## Number of DEGs shared across various thresholds

```{r fig.height=8, fig.width=10}
combs <- 
  t(combn(tissue.order,2)) %>% data.frame() %>% rename(Var1=X1, Var2=X2)

library(foreach)
res <- foreach(thre1=seq(0.1, 0.4, 0.01), .combine = "rbind") %do% {
  x <- foreach(thre2=c(0.1, 0.2, 0.3, 0.4), .combine = "rbind") %do% {
    tmp <- 
      rbind(
      filter(deg.si.markers, avg_log2FC>thre1, pct.1>thre2) ,
      filter(deg.colon.markers, avg_log2FC>thre1, pct.1>thre2) %>% 
        mutate(cluster=str_replace_all(cluster, c("Colon_1"="Colon proximal", "Colon_2"="Colon medial", "Colon_3"="Colon distal")))
    ) %>% select(cluster,gene)
  
    data.frame(combs, 
               pct.1 = paste0("pct1= ",thre2),
               n=apply(combs,1,function(x) length(intersect(filter(tmp, cluster==x[1]) %>% pull(gene), filter(tmp, cluster==x[2]) %>% pull(gene)))),
               union=apply(combs,1,function(x) length(union(filter(tmp, cluster==x[1]) %>% pull(gene), filter(tmp, cluster==x[2]) %>% pull(gene)))),
               gene.list=apply(combs,1,function(x) 
                 paste0(intersect(filter(tmp, cluster==x[1]) %>% pull(gene), filter(tmp, cluster==x[2]) %>% pull(gene)), collapse = ","))
               )
  }
  x$log2FC <- thre1
  x
}

unite(res,  col="region", sep="+",  Var1, Var2) %>% 
ggplot(aes(x=log2FC, y=n/union, col=region)) + 
  geom_point(size=1)  +
  geom_smooth(se=F) +
  labs(x="log2FC threshold", y="# of DEGs") +
  facet_wrap(~pct.1)  +
  theme_classic() + 
  theme(strip.background = element_blank())
```

## Number of DEGs shared between SI and colon at fixed thresholds

```{r fig.height=3.5, fig.width=9}
tmp <- 
  rbind(
  filter(deg.si.markers, avg_log2FC>0.25, pct.1>0.3) ,
  filter(deg.colon.markers, avg_log2FC>0.25, pct.1>0.3) %>% 
    mutate(cluster=str_replace_all(cluster, c("Colon_1"="Colon proximal", "Colon_2"="Colon medial", "Colon_3"="Colon distal")))
) %>% select(cluster, gene, avg_log2FC) 

m <- make_comb_mat(lapply(split(tmp, tmp$cluster), function(x)x$gene))

## Tag:Fig1b
ht = draw(UpSet(m,  
          bg_col = rev(colors),
          set_order = c("Duodenum", "Jejunum1", "Jejunum2", "Ileum",
                     "Colon proximal", "Colon medial", "Colon distal")))
od = column_order(ht)
cs = comb_size(m)
decorate_annotation("intersection_size", {
    grid.text(cs[od], x = seq_along(cs), y = unit(cs[od], "native") + unit(2., "pt"), 
        default.units = "native", just = "bottom", gp = gpar(fontsize = 10))
})

```




```{r}
sessionInfo()
```

