# Description of data

```
data/
├── celiac_risk_genes.csv.gz                                    ## Celiac disease related genes
├── diverticular_genes.xlsx                                     ## Diverticular disease related genes
├── dss_deg.d12_vs_d0.csv.gz                                    ## DEGs between D12 and D0 (sham) in DSS experiment
├── dss_deg.d30_vs_d0.csv.gz                                    ## DEGs between D30 and D0 (sham) in DSS experiment
├── dss_deg.d73_vs_d0.csv.gz                                    ## DEGs between D73 and D0 (sham) in DSS experiment
├── dss_deg.ido1_pos_vs_neg_logfc0.csv.gz                       ## Gene expression fold change between Ido1+ and Ido1- spots in DSS experiment
├── dss_fc.d12_vs_d0.csv.gz                                     ## Gene expression fold change between D12 and D0 (sham) in DSS experiment
├── dss_fc.d30_vs_d0.csv.gz                                     ## Gene expression fold change between D30 and D0 (sham) in DSS experiment
├── dss_fc.d73_vs_d0.csv.gz                                     ## Gene expression fold change between D73 and D0 (sham) in DSS experiment
├── ibd_gene_curated.txt.gz                                     ## Curated IBD risk genes
├── ibd_risk_genes.csv.gz                                       ## IBD risk genes
├── iMusta4SLC.csv.gz                                           ## SLC and ligand annotation
├── marker_genes_tissue_jmerged_deg.csv.gz                      ## DEGs for each intestine section (SI+colon) compared to other (jejunum merged)
├── marker_genes_tissue_si_deg.csv.gz                           ## DEGs for each small intestine section compared to other (jejunum split into two)
├── marker_genes_tissue_subseg_deg.csv.gz                       ## DEGs for each tissue subsection (split on unrolled X) vs other subsections in the same tissue
├── microbe_deg.fmt_vs_gf.csv.gz                                ## DEGs between FMT and GF mice
├── microbe_deg.spf_vs_fmt.csv.gz                               ## DEGs between SPF and FMT mice
├── microbe_deg.spf_vs_gf.csv.gz                                ## DEGs between SPF and GF mice
├── microbe_fc.fmt_vs_gf.csv.gz                                 ## Gene expression fold change between FMT and GF mice
├── microbe_fc.spf_vs_fmt.csv.gz                                ## Gene expression fold change between SPF and FMT mice
├── microbe_fc.spf_vs_gf.csv.gz                                 ## Gene expression fold change between SPF and GF mice
├── monogenic_ibd_gene_list_Wei_2022.xlsx                       ## Monogenic IBD genes curated
├── Mouse_GPCRs_v05012023.xlsx                                  ## Mouse GPCR genes
├── Mouse_TFs.txt.gz                                            ## Mouse transcription factor genes
├── nakata_et_al_epi_markers.tsv.gz                             ## Marker genes for each epithelial cell type from doi:10.1126/scitranslmed.adg5252
├── paper_dss_rolls.rds                                         ## Master dataset for all the Visium samples for DSS experiment
├── paper_gf_light_vs_gf_dark_311A_313C.spearman_corr.csv.gz    ## Spearman correlation of gene expression and the unrolled X axis. Light vs dark
├── paper_sham_vs_DSS12.spearman_corr.csv.gz                    ## Spearman correlation of gene expression and the unrolled X axis. D0 vs D12
├── paper_sham_vs_DSS30.spearman_corr.csv.gz                    ## Spearman correlation of gene expression and the unrolled X axis. D0 vs D30
├── paper_sham_vs_DSS73.spearman_corr.csv.gz                    ## Spearman correlation of gene expression and the unrolled X axis. D0 vs D73
├── paper_spf_gf_fmt_colon_rolls.spearman_corr.csv.gz           ## Spearman correlation of gene expression and the unrolled X axis. Colon rolls
├── paper_spf_gf_fmt_each_roll.spearman_corr.csv.gz             ## Spearman correlation of gene expression and the unrolled X axis. Each roll
├── paper_spf_gf_fmt_rolls.rds                                  ## Master dataset for all the Visium samples for SPF/GF/FMT mice.
├── paper_spf_gf_fmt_SI_rolls.spearman_corr.csv.gz              ## Spearman correlation of gene expression and the unrolled X axis. SI concatenated
├── paper_spf_light_vs_spf_dark_311C_313D.spearman_corr.csv.gz  ## Spearman correlation of gene expression and the unrolled X axis. SPF mice in light and dark
├── segment_shared_genes.csv.gz                                 ## Genes used in the Upset plot, highlighting the DEGs shared across SI and colon
├── slc_selected_anno.csv.gz                                    ## Manually annotated SLC ligand types
├── unroll_aligned_w_seg.csv.gz                                 ## Unrolled coordniates of SPF/GF/FMT mouse rolls. Aligned and split into discrete segments
└── unroll_aligned_w_seg_dss.csv.gz                             ## Unrolled coordniates of DSS treated mouse rolls. Aligned and split into discrete segments
```