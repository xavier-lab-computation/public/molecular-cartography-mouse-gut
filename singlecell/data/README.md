# Description of data

```
data/
├── 20240219_unified_annotated.diet.rds                                    ## Master RDS file for all single cell samples
├── enterocyte_hires_umap.rds                                              ## UMAP data for enterocyte types
├── fibroblast_hires_umap.rds                                              ## UMAP data for fibroblast cell types
├── goblet_hires_umap.rds                                                  ## UMAP data for goblet cell types
├── lineage_annotation.csv.gz                                              ## Annotation for each cell type at different resolutions
├── marker_genes_goblet_hi_res.csv.gz                                      ## DEGs for each goblet cell subtypes compared with other goblet cells
├── marker_genes_Igfbp3POS_Serpina3gHI_vs_Igfbp3POS_others.csv.gz          ## DEGs between Igfbp3+ Serpina3g-hi fibroblast and other Igfbp3+ fibroblast cells
├── marker_genes_Mature_enterocyteII_vs_mature_enterocyte_other.csv.gz     ## DEGs between mature enterocyte II and other mature enterocytes
├── spf_vs_gf_celltype_segment.csv.gz                                      ## DEGs between SPF and GF for each cell type in each segment (A, B, C, D)
└── velocyto_data.rds                                                      ## Data generated from velocyto
```


